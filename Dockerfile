# Build as haltian/openocd
FROM buildpack-deps
MAINTAINER Roman Saveljev <roman.saveljev@haltian.com>

RUN \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-key update && \
    apt-get update && \
    apt-get -y install build-essential git libncurses5-dev \
        flex bison libtool autoconf pkg-config automake \
        texinfo libusb-1.0 libftdi-dev libftdi1

ADD . /opt/openocd
RUN \
    cd /opt/openocd && \
    ./bootstrap && \
    ./configure --enable-maintainer-mode --enable-ftdi --enable-legacy-ft2232_libftdi && \
    make -j4 && make install && \
    : Still wipes all build intermediates && \
    rm -rf /opt/openocd
    
